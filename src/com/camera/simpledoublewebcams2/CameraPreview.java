package com.camera.simpledoublewebcams2;

import java.io.File;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.R.string;
import android.content.Context;
import android.os.Environment;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;

class CameraPreview extends SurfaceView implements SurfaceHolder.Callback, Runnable {

	private static final boolean DEBUG=true;
	private static final String TAG="DoubleWebCam";
	protected Context context;
	private SurfaceHolder holder;
    Thread mainLoop = null;
	private Bitmap[] bmp=new Bitmap[2];
	private boolean[] cameraExists=new boolean[2];
	private boolean shouldStop=false;
	
	// Size of each image. This definition also exists in jni/ImageProc.h
	public static final int IMG_WIDTH=1280;
	public static final int IMG_HEIGHT=720;
    
	public int winWidth=0;
	public int winHeight=0;
	public Rect rect1, rect2;
	public String type_mode;
	public int set_fps = 30;
    public native void pixeltobmp(Bitmap bitmap1, Bitmap bitmap2);
    public TextView text;
    // prepareCamera selects the device automatically. please set videoid=0
    public native int prepareCamera(int videoid);
    // prepareCameraWithBase is used if you want to specify the device manually.
    // e.g., for /dev/video[1,2], use prepareCameraWithBase(0,1).
    // please set videoid=0
    public native int prepareCameraWithBase(int videoid, int videobase);
    public native void processCamera();
    public native void processRBCamera(int lrmode);
    public native void stopCamera();

    static {
    	System.loadLibrary("jpeg");
        System.loadLibrary("ImageProc");
    }
	public CameraPreview(Context context) {
		super(context);
		this.context = context;
		if(DEBUG) Log.d(TAG,"CameraPreview constructed");
		setFocusable(true);

		bmp[0] = null;
		bmp[1] = null;

		holder = getHolder();
		holder.addCallback(this);
		//holder.setType(SurfaceHolder.SURFACE_TYPE_NORMAL);	
	}

	public CameraPreview(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		if(DEBUG) Log.d(TAG,"CameraPreview constructed");
		setFocusable(true);

		bmp[0] = null;
		bmp[1] = null;

		holder = getHolder();
		holder.addCallback(this);
		//holder.setType(SurfaceHolder.SURFACE_TYPE_NORMAL);	
	}
	
	public void setType(String str){
		type_mode = str;
	}
	
    @Override
    public void run() {
    	long beforetime = System.currentTimeMillis();
    	float fps = 30;
        while (true && (cameraExists[0]||cameraExists[1])) {
        	long ctime = System.currentTimeMillis();
        	long dt  =ctime - beforetime;
        	beforetime = ctime;
        	if(dt >=.00001)
         	fps += .02*((1000/dt)-fps);
        	
        	processCamera();
        	pixeltobmp(bmp[0],bmp[1]);
        	//Date date = new Date();
        	//File directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + File.separator + "Webcam");
    		//directory.mkdirs();
        	//String filename = directory.getAbsoluteFile() + File.separator + String.valueOf(date.getTime()) + ".jpg";
        	//Log.d("type "," type is "+type_mode);
        	//try {
     	      //FileOutputStream out = new FileOutputStream(filename);
     	      //bmp[0].compress(Bitmap.CompressFormat.JPEG, 90, out);
     	    //out.flush();
     	   // out.close();
     		//} catch (Exception e) {
     		       //e.printStackTrace();
     		//}
        	Canvas canvas = getHolder().lockCanvas();
            if (canvas != null)
            {
            	if(winWidth==0){
            		winWidth=this.getWidth();
            		winHeight=this.getHeight();
            		rect1 = new Rect((int) (0.1*winWidth), (int) (0.1*winHeight), (int) (0.9*winWidth-1), winHeight/2-1);
            		//rect2 = new Rect(winWidth/2,0,winWidth-1, winWidth*3/4/2-1);
            	}

        		canvas.drawBitmap(bmp[0],null,rect1,null);
        		//canvas.drawBitmap(bmp[1],null,rect2,null);

            	getHolder().unlockCanvasAndPost(canvas);
            }
            if(shouldStop){
            	shouldStop = false;  
            	break;
            }
            /*mBtn = ((Button) findViewById( R.id.cp ));
            mBtn.setOnClickListener( new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mBtn.setText("Changed");
					mBtn.setTextColor( Color.RED );
				}
            });*/
        	//Log.e("Time ", " time is   "+ time);
        	//Log.e("Beforetime","before time is"+ beforetime);

        	Log.e("FPS ", " frames per second  "+ fps);
        	set_fps(fps);
        	text = (TextView) findViewById(R.id.fps);
        	//fps = cp.get_fps();
        	//text.setText("fps");

        }
    }

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		if(DEBUG) Log.d(TAG, "surfaceCreated");
		for(int i=0 ; i<1 ; i++){
			if(bmp[i]==null){
				bmp[i] = Bitmap.createBitmap(IMG_WIDTH, IMG_HEIGHT, Bitmap.Config.ARGB_8888);
			}
		}

		// /dev/video[0,1] are used.
		// In some omap devices, /dev/video[0..3] are used by system,
		// and, in such a case, /dev/video[4,5] are selected automatically.
		int ret = prepareCamera(0);
		
		// ret: -3(/dev/video[01] do not exist)
		//      -2(/dev/video1 does not exist)
		//      -1(/dev/video0 does not exist)
		ret = -ret;
		//if((ret & 0x2)>>1==1){
			//cameraExists[1] = false;
		//}else{
			//cameraExists[1] = true;
		//}
		if((ret & 0x01)==1){
			cameraExists[0] = false;
		}else{
			cameraExists[0] = true;
		}
		
        mainLoop = new Thread(this);
        mainLoop.start();		
	}
	
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		if(DEBUG) Log.d(TAG, "surfaceChanged");
	}
	
	public void set_fps(float fps)
	{
		for (FpsListener fpsl : fpslistners) {
			fpsl.onNewFps(Math.round(fps));
		}
		set_fps = (int)fps;
	}
	
	public int get_fps()
	{
		return set_fps;
	}
	

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		if(DEBUG) Log.d(TAG, "surfaceDestroyed");
		if(cameraExists[0]||cameraExists[1]){
			shouldStop = true;
			for(int i=0 ; i<10 ; i++){
				try{ 
					Thread.sleep(100); // wait for thread stopping
				}catch(Exception e){}
				if(!shouldStop){
					break;
				}
			}
			stopCamera();
		}
	}
	
	
	List <FpsListener> fpslistners = new ArrayList<FpsListener>();
	
	public void addListner(FpsListener fpsListener)
	{
		fpslistners.add(fpsListener);	
	}
	
	public void deleteListner(FpsListener fpsl)
	{
		fpslistners.remove(fpsl);	
	}
	
}
