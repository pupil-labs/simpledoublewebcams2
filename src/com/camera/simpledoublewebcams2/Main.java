package com.camera.simpledoublewebcams2;

import android.app.Activity;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Chronometer;
import android.widget.TextView;

public class Main extends Activity {
	
	private CameraPreview cp;
	private int flag = 0;
	long time = 0;
	private Chronometer myChronometer;
	private TextView text;
	private int fps = 30;
	long starttime = System.currentTimeMillis();
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		Log.e("Mayank","In oncreate function");
		//myChronometer = (Chronometer)findViewById(R.id.chronometer);
		cp = (CameraPreview) findViewById(R.id.cp);
		//myChronometer.start();
    	text = (TextView) findViewById(R.id.fps);
    	cp.addListner(new FpsListener() {
			
			public void onNewFps(final int fps) {
				runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						text.setText(fps+" ;fps");
					}
				});
				
			}
		});
    	//fps = cp.get_fps();
    	//text.setText("fps");
		
	}	
	public void set_text(View v) {
		myChronometer = (Chronometer)findViewById(R.id.chronometer);
		if(flag%2==0){
		myChronometer.setBase(SystemClock.elapsedRealtime());
	
		myChronometer.start();
		}
		if(flag%2 == 1){
			myChronometer.setBase(SystemClock.elapsedRealtime());
			//cp.setType("Preview");
			myChronometer.stop();
		}
		flag ++;
	
	}
}

